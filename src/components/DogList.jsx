import "./DogList.css"
import { useContext } from "react";
import DogListItem from "./DogListItem";
import { DogContext } from "../hoc/DogContext";
import keycloak from "../keycloak/keycloak";

function DogList () {
    
    const { listDogs, addDog } = useContext(DogContext)

    const handleAddDogInternet = () => {
        addDog()
    }

    return (
        <>
            <h2>Dogs up for Adoption 🐕</h2> 
            { keycloak.authenticated && keycloak.hasRealmRole("ADMIN") && 
                <div className="add-dogs">
                    <button onClick={ handleAddDogInternet }>Find Internet dog! 🕸</button>
                </div>
            }
            { listDogs.map( (item,index)=> <DogListItem dog={ item } key={ index } /> )}
        </>
    )
}

export default DogList;