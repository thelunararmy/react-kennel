import { boost } from "./actions";


export const counterMiddleware = ({dispatch}) => next => action => {
    next(action);

    // Logical stuff
    if (action.type === "[counter] BOOST") {
        setTimeout(() => {
            console.log("Charging hyperdrive,... Sending the counter to the moooooooon")
            dispatch( boost(900) )
        }, 1000)
    }
}