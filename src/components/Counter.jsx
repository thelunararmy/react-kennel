import { useSelector, useDispatch } from "react-redux";
import { boost, decrement, increment } from "../reduxstore/actions";
import { getCounter } from "../reduxstore/selectors";

function Counter() {
    const counter = useSelector(getCounter)
    const dispatch = useDispatch()
    const handleIncrement = () => dispatch( increment() );
    const handleDecrement = () => dispatch( decrement() );
    const handleBoost = () => dispatch( boost(5) );

    return (
        <>
            <h2>Counter Page</h2>
            <div>
                <span>Current value of [Counter] is: <b>{counter}</b></span>
            </div>
            <div>
                <button onClick={ handleIncrement }>Increase</button>
                <button onClick={ handleDecrement }>Decrease</button>
                <button onClick={ handleBoost }>🚀</button>
            </div>
        </>
    )
}

export default Counter;