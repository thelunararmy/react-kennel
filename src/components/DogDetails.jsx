import { useContext, useState } from "react"
import { useForm } from "react-hook-form";
import { DogContext } from "../hoc/DogContext"

function DogDetails() {

    const { listDogs, setListDogs } = useContext(DogContext)
    const [selectedDog, setSelectedDog] = useState(); // Controlled
    const { register, handleSubmit } = useForm();

    const handleDogSelected = event => { // Controlled
        event.preventDefault();
        setSelectedDog(event.target.value)
    }

    const onEditSubmit = data => {
        let { name, city, country } = data
        name = name !== "" ? name : theDog.name
        city = city !== "" ? city : theDog.city
        country = country !== "" ? country : theDog.country
        
        // eslint-disable-next-line eqeqeq
        const filteredDogList = listDogs.filter(x=>x.id != selectedDog)
        const updatedDogList = [...filteredDogList, {...theDog, name, city, country}]
        setListDogs(updatedDogList)
    }

    // eslint-disable-next-line eqeqeq
    const theDog = selectedDog ? listDogs.filter(x=>x.id == selectedDog)[0] : undefined

    return (
        <div>
            <h2>My Dog Form</h2>
            { listDogs.length > 0 
                ? <div> {/* Controlled Component */}
                    <span>Select a dog to edit: </span>
                    <select name="dog-selector" onChange={ handleDogSelected } value={ selectedDog }>
                        { listDogs.map( (dog, index) => <option value={dog.id} key={index}>{dog.name}</option> ) }
                    </select>
                </div>
                : <div>
                    <span>Currently no dogs in the kennel. 🎊</span>
                </div>
            }
            { theDog && 
                <div name="edit-form">  {/* Hook form! */}
                    <div>
                        <img src={theDog.picture} alt="dog" width="200px"></img>
                        <div>
                            <span>Name  </span>
                            <input name="name" {...register("name")} placeholder={theDog.name}/>
                        </div>
                        <div>
                            <span>City  </span>
                            <input name="city" {...register("city")} placeholder={theDog.city}/>
                        </div>
                        <div>
                            <span>Country  </span>
                            <input name="country" {...register("country")} placeholder={theDog.country}/>
                        </div>
                        <button onClick={handleSubmit(onEditSubmit)}>Submit Edit 📋</button>
                    </div>
                </div>
            }            
        </div>
    )
}
export default DogDetails