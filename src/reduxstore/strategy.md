1. Install Redux, toolkit, browser extention ✔️
2. Create a store ✔️
3. Create provider for the store ✔️
4. Create actions ✔️
5. Create a reducer (based on actions) ✔️
6. Add reducer to our store ✔️
7. Testing (browser extensions) ✔️
8. Create a component ✔️
9. Add a selector to the component (getter) ✔️
10. Add a dispatch to the component (set) ✔️
11. Create 1 Middleware
12. Done, :)