import { BrowserRouter, Routes, Route, Link } from 'react-router-dom';
import './App.css';
import Catalog from './components/Catalog';
import Counter from './components/Counter';
import DogDetails from './components/DogDetails';
import DogList from './components/DogList';
import TokenPage from './components/TokenPage';
import LoggedinRoute from './hoc/LoggedInRoute';
import keycloak from './keycloak/keycloak';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <h1>Happy Hound Kennel</h1>
        <div className='dog-gifs'>
          { [0,1,2,3,4,5].map( (number) => <img src="./images/dog_1_sprite.webp" alt={`dog_${number}`} key={`dog_${number}`}></img> )}
        </div>
        <nav>
          <li><Link to="/list">Goto Dog List 🐕</Link> </li>
          <li><Link to="/catalog">Goto Catalog 🖼</Link> </li>
          <li><Link to="/details">Goto Details 🐕‍🦺</Link> </li>
          <li><Link to="/counter">Goto Counter 🧮</Link> </li>
          <li><Link to="/token">Goto Token ♟️</Link> </li>
          { keycloak.authenticated 
          ? <li><button onClick={ () => { keycloak.logout() } }>Logout 👋</button></li>
          : <li><button onClick={ () => { keycloak.login() } }>Login 🔐</button></li> }
          { keycloak.authenticated && keycloak.tokenParsed && 
          <li>Hello <b>{ keycloak.tokenParsed.name }</b></li>}
        </nav>
        <Routes>
          <Route path="/list" element= { <DogList /> } />
          <Route path="/catalog" element= { <Catalog />  } />
          <Route path="/details" element= { <DogDetails />  } />
          <Route path="/counter" element= { <Counter />  } />
          <Route path="/token" element= { 
            <LoggedinRoute>
              <TokenPage /> 
            </LoggedinRoute>
          } />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
