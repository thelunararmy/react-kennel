# Happy Hound Kennel App
A demo app anyone can `git clone` and play around with. The core design principal of the app can be seen in `component_tree_plan.png`.

## Install
```
git clone https://gitlab.com/thelunararmy/react-kennel
cd react-kennel
npm install
```

## Usage
```
npm start
``` 
This will open a new Webpage in your browser at `localhost:3000`. Remember to use your React and Redux browser extentions

## Contributors

* [JC Bailey (@thelunaramy)](@thelunararmy)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
Noroff Accelerate, 2022.
