import { composeWithDevTools } from "@redux-devtools/extension";
import { counterMiddleware } from "./middleware";
import { counterReducer } from "./reducers";

const { createStore, combineReducers, applyMiddleware } = require("redux");

const appReducer = combineReducers({ 
    counter: counterReducer 
})

const middlewares = applyMiddleware(
    counterMiddleware
)

const store = createStore(appReducer, composeWithDevTools(middlewares));

export default store;